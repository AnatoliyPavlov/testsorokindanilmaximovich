using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    private Vector3 startPosition = Vector3.zero;
    private Vector3 endPosition;
    private float timer;
    private float endTime;

    private void Update()
    {
        if (timer < endTime)
        {
            timer += Time.deltaTime;
            transform.position = Vector3.Lerp(startPosition, endPosition, timer / endTime);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void StartMove(int speed, int distance)
    {
        endPosition = new Vector3(0, 0, distance);
        endTime = (float)distance / speed;

        CubeReset();
    }

    private void CubeReset()
    {
        transform.position = startPosition;
        timer = 0;

        gameObject.SetActive(true);
    }
}