using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private CubeSettingsInput cubeSettingsInput;
    [SerializeField] private CubeSpawner cubeSpawner;

    private float timer = 0f;

    private void Update()
    {
        timer += Time.deltaTime;

        if (cubeSettingsInput.IsCanCreateCube && cubeSettingsInput.TimeSpawn < timer)
        {
            timer = 0f;

            cubeSpawner.CubeSpawn(cubeSettingsInput.Speed, cubeSettingsInput.Distance);
        }
    }
}