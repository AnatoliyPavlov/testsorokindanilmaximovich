using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubePool<T> where T : MonoBehaviour
{
    public T prefab { get; }
    public bool autoExpand { get; set; } //����� �������������� ����
    public Transform container { get; } //��������� ������ �������� ����� ����������� �������
    private List<T> pool; //������ ����

    public CubePool(T prefab, int count, Transform container)
    {
        this.prefab = prefab;
        this.container = container;
        this.CreatePool(count);
    }

    public T GetFreeElement()
    {
        if (this.HasFreeElement(out var element))
            return element;

        if (this.autoExpand)
            return this.CreateObject(true);

        throw new Exception($"There is no free elements in pool of type {typeof(T)}");
    }

    private bool HasFreeElement(out T element)
    {
        foreach (var mono in pool)
        {
            if (!mono.gameObject.activeInHierarchy)
            {
                element = mono;
                mono.gameObject.SetActive(true);
                return true;
            }
        }
        element = null;

        return false;
    }

    private void CreatePool(int count)
    {
        this.pool = new List<T>();

        for (int i = 0; i < count; i++)
            this.CreateObject();
    }

    private T CreateObject(bool isActiveByDefault = false)
    {
        var createdObject = UnityEngine.Object.Instantiate(this.prefab, this.container);
        createdObject.gameObject.SetActive(isActiveByDefault);
        this.pool.Add(createdObject);

        return createdObject;
    }
}