using System;
using TMPro;
using UnityEngine;

public class CubeSettingsInput : MonoBehaviour
{
    [SerializeField] private TMP_InputField TimeCubeSpawnInputField;
    [SerializeField] private TMP_InputField CubeSpeedInputField;
    [SerializeField] private TMP_InputField CubeDistanceInputField;

    public int TimeSpawn { get { return timeSpawn; } }
    public int Speed { get { return speed; } }
    public int Distance { get { return distance; } }
    public bool IsCanCreateCube { get { return CheckIsCanCreateCube(); } }

    private int timeSpawn = 0;
    private int speed = 0;
    private int distance = 0;

    private void OnEnable()
    {
        TimeCubeSpawnInputField.onValueChanged.AddListener(OnTimeValueChanged);
        CubeSpeedInputField.onValueChanged.AddListener(OnSpeedValueChanged);
        CubeDistanceInputField.onValueChanged.AddListener(OnDistanceValueChanged);
    }

    private void OnTimeValueChanged(string value)
    {
        try
        {
            int timeSpawn = Convert.ToInt32(value);

            if (timeSpawn < 1)
            {
                TimeCubeSpawnInputField.text = "1";

                return;
            }

            this.timeSpawn = timeSpawn;
        }
        catch(Exception) { }
    }

    private void OnSpeedValueChanged(string value)
    {
        try
        {
            int speed = Convert.ToInt32(value);

            if (speed < 1)
            {
                CubeSpeedInputField.text = "1";

                return;
            }

            this.speed = speed;
        }
        catch(Exception) { }
    }

    private void OnDistanceValueChanged(string value)
    {
        try
        {
            int distance = Convert.ToInt32(value);

            if (speed < 1)
            {
                CubeDistanceInputField.text = "1";

                return;
            }

            this.distance = distance;
        }
        catch(Exception) { }
    }

    private void OnDisable()
    {
        TimeCubeSpawnInputField.onValueChanged.RemoveListener(OnTimeValueChanged);
        CubeSpeedInputField.onValueChanged.RemoveListener(OnSpeedValueChanged);
        CubeDistanceInputField.onValueChanged.RemoveListener(OnDistanceValueChanged);
    }

    private bool CheckIsCanCreateCube()
    {
        return timeSpawn > 0
            && speed > 0
            && distance > 0;
    }
}