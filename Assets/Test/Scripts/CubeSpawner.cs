using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeSpawner : MonoBehaviour
{
    [SerializeField] private Cube cubePrefab;
    [SerializeField] private Transform cubeCreateContainer;
    [SerializeField] private int poolCount;

    private CubePool<Cube> cubePool;

    private void Awake()
    {
        if (cubePool == null)
        {
            cubePool = new CubePool<Cube>(cubePrefab, poolCount, cubeCreateContainer.transform);
            cubePool.autoExpand = true;
        }
    }

    public void CubeSpawn(int speed, int distance)
    {
        var cube = cubePool.GetFreeElement();

        cube.StartMove(speed, distance);
    }
}